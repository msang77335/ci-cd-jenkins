#!/bin/bash

echo "=============== INSTALL DOCKER ==============="

echo "=============== STEP 1 ==============="
sudo apt-get update

echo "=============== STEP 2 ==============="
sudo apt-get update

echo "=============== STEP 3 ==============="
yes | sudo apt-get install curl apt-transport-https ca-certificates software-properties-common

echo "=============== STEP 4 ==============="
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

echo "=============== STEP 5 ==============="
yes "" | sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

echo "=============== STEP 6 ==============="
sudo apt update

echo "=============== STEP 7 ==============="
apt-cache policy docker-ce

echo "=============== STEP 8 ==============="
yes | sudo apt-get install docker-ce

echo "=============== STEP 9 ==============="
sudo systemctl status docker -l --no-pager

echo "=============== STEP 10 ==============="
sudo usermod -aG docker $(whoami)

echo "=============== JENKINS ==============="

echo "=============== STEP 1 ==============="
docker pull msang77335/jenkins

echo "=============== STEP 2 ==============="
docker stop jenkins && docker rm jenkins

echo "=============== STEP 3 ==============="
docker run -d --name jenkins -p 80:8080 -p 50000:50000 -v jenkins_home:/var/jenkins_home -v /var/run/docker.sock:/var/run/docker.sock msang77335/jenkins

echo "=============== STEP 4 ==============="
docker exec -i jenkins sh << EOF
    echo "=============== STEP 5 ==============="
    # Run Step 5 command inside the container
    docker buildx create --name mybuilder --use --bootstrap
EOF










